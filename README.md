# Santa's Presents Brawl #

A one-weekend project that I made to get to know a little javascript.

Finished game can be played on [clay.io](http://clay.io/game/santabrawl)

### Credits ###
* Audio library: [jsfxr](https://github.com/mneubrand/jsfxr)
* Some code (e.g. utils and key-event handling) was taken from [Radius Raid](https://github.com/jackrugile/radius-raid-js13k)

### License ###
Licensed under MIT License. For more information see LICENSE file.
jsfxr licensed under Apache License 2.0. For more information see jsfxr-license file.

### Screenshot ###

![promo.png](https://bitbucket.org/repo/9RBEgd/images/87567666-promo.png)