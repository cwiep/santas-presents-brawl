$.Chimney = function(x,y) {
    //this.x= $.utils.rand(0, $.canvas.width-64);
    this.x = x;
    this.y = y;
    this.state = 0;
    this.animTimer = 10;
    this.cloudTime = $.utils.rand(300, 450);
    this.cloudTimer = this.cloudTime;
};

$.Chimney.prototype.update = function() {
    this.animTimer--;
    this.cloudTimer--;
    if(this.animTimer < 0) {
        this.state = 0;
    }
    if(this.cloudTimer < 0) {
        this.cloudTimer = this.cloudTime;
        this.produceCloud();
    }
};

$.Chimney.prototype.render = function() {
    // only render if chimney is visible
    if($.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x, this.y) || 
       $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x+64, this.y) || 
       $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x, this.y+64) || 
       $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x+64, this.y+64) ) {
        $.ctx.drawImage($.chimneyImage,this.state*64,0,64,64, this.x-$.cameraX, this.y-$.cameraY,64,64);
    }
};

$.Chimney.prototype.produceCloud = function() {
    $.clouds.push( new $.Cloud(this.x, this.y, this.y-32) );
    this.animTimer = 10;
    this.state = 1;
};
