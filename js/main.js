window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame   ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame    ||
      window.oRequestAnimationFrame      ||
      window.msRequestAnimationFrame     ||
      function(/* function */ callback, /* DOMElement */ element){
        window.setTimeout(callback, 1000 / 60);
      };
})();
	
////////////////////////////////// GAME
$.init = function() {
    $.canvas = document.getElementById("canvas");
    $.ctx = $.canvas.getContext("2d");

    $.background = new Image();
    $.background.src = "res/bg.png";
    
    $.chimneyImage = new Image();
    $.chimneyImage.src = "res/chimney.png";
    
    $.cloudImage = new Image();
	$.cloudImage.src = "res/cloud.png";
    
    $.arrows= new Image();
    $.arrows.src = "res/arrows.png";
    
    $.clock= new Image();
    $.clock.src = "res/clock.png";
    
    $.normalFont = "bold 16px sans-serif"
    $.plusFont = "bold 20px sans-serif"
    $.ctx.font = $.normalFont;

    $.resetGame();
    
    $.keys = {
            state: {
                    up: 0,
                    down: 0,
                    left: 0,
                    right: 0,
                    space: 0,
                    p: 0
            },
            pressed: {
                    up: 0,
                    down: 0,
                    left: 0,
                    right: 0,
                    space: 0,
                    p: 0
            }
    };
    $.okeys = {};

    window.addEventListener( 'keydown', $.keydowncb );
    window.addEventListener( 'keyup', $.keyupcb );

    $.gameState = 'menu';
    
    $.utils.renderFavicon();
    $.lastTime = (new Date()).getTime();
    $.loop();
}

$.resetGame = function() {
    $.bgy = 0;
    $.clouds = [];

	$.cameraX = 0;
	$.cameraY = 0;
    
    $.level = new $.Level();
    
    $.curGift = 0;
    $.curChimney = 0;
    
    $.gift = new $.Gift();
    $.gift.reset();
    
    $.santa = new $.Santa();
    
	$.points = {
        value: 0,
        rumble: 0,
        x: 500,
        y: 40,
        rx: 0,
        ry: 0,
        rtimer: 0,
        pointState: 'gain',
        losePoint: function() {
            this.value--;
            if(this.value < 0) this.value = 0;
            this.pointState = 'lose';
            this.rumble += 20;
            $.audio.playSound($.audio.sounds['minuspoint']);
        },
        gainPoint: function() {
            this.value++;
            this.pointState = 'gain';
            this.rumble += 20;
            $.audio.playSound($.audio.sounds['point']);
        }
    };

    $.time = 60;
    
    $.fblock = false;
    $.showFps = false;
    $.spaceBlock = false;
    $.jumpblock = false;
}

$.loop = function() {
    var t = (new Date()).getTime();
    var dt = t - $.lastTime;
    $.time -= dt/1000;
    $.lastTime = t;
    $.update();
    $.handleInput();
	$.checkScreenBounds();
    $.render(1000/dt);
    requestAnimFrame( $.loop );
}

$.update = function() {
    $.bgy += 2;
    if($.gameState === 'menu') {
        $.menu.update();
    } 
    if($.gameState === 'game'){
        if($.time <= 0) {
            $.gameState = 'score';
        }
        
        $.santa.update();
        $.level.update();
        $.gift.update();
        
        if($.points.rumble > 0) {
            $.points.rumble--;
            $.points.rumble = ($.points.rumble < 0) ? 0 : $.points.rumble;
            $.points.rx = $.utils.rand(-$.points.rumble, $.points.rumble);
            $.points.ry = $.utils.rand(-$.points.rumble, $.points.rumble);
        } else {
            $.points.rx = 0;
            $.points.ry = 0;
        }
    }
}

$.checkScreenBounds = function() {
    if($.santa.x <= 0) {
        $.santa.x = 0;
    }
    
    if($.santa.x+64 >= $.level.pxWidth) $.santa.x = $.level.pxWidth-64;
    if($.santa.y+64 >= $.level.pxHeight) $.santa.y = $.level.pxHeight-64;

	// update camera
	$.cameraX = $.santa.x - $.canvas.width / 2;
	$.cameraY = $.santa.y - $.canvas.height / 2;
	
	if($.cameraX < 0) $.cameraX = 0;
    if($.cameraY < 0) $.cameraY = 0;
	if($.cameraX+600 > $.level.pxWidth) $.cameraX = $.level.pxWidth-600;
    if($.cameraY+360 > $.level.pxHeight) $.cameraY = $.level.pxHeight-360;
}

$.render = function(fps) {
    if($.gameState === 'menu') {
        $.drawBackground();
        $.menu.render();
    } 
    if($.gameState === 'game') {
        $.drawBackground();
        for( c in $.clouds ) {
            $.clouds[c].render();
        }
        $.level.render();
        $.santa.render();
        $.gift.render();
        
        if($.gift.state === 'carried') {
            var ax = $.level.chimneys[$.curChimney].x+16;
            var ay = $.level.chimneys[$.curChimney].y-32-16;
            $.ctx.drawImage($.arrows, 0,0,32,32,ax-$.cameraX, ay-$.cameraY, 32, 32);
            
            if(ax < $.cameraX) {
                $.ctx.drawImage($.arrows,32,0,32,32, 10, 150-16, 32, 32);
            }
            if(ax > $.cameraX + $.canvas.width) {
                $.ctx.drawImage($.arrows,64,0,32,32, 600-42, 150-16, 32, 32);
            }
        } else {
            if($.gift.state === 'resting') {
                var ax = $.gift.x+16;
                var ay = $.gift.y-32-16;
                $.ctx.drawImage($.arrows, 0,0,32,32,ax-$.cameraX, ay-$.cameraY, 32, 32);
            }
            
            if($.gift.x< $.cameraX) {
                $.ctx.drawImage($.arrows,32,0,32,32, 10, 150-16, 32, 32);
            }
            if($.gift.x > $.cameraX + $.canvas.width) {
                $.ctx.drawImage($.arrows,64,0,32,32, 600-42, 150-16, 32, 32);
            }
        }
        
        // draw interface
        if($.showFps) {
            $.drawText("FPS: " + fps, 10, 20, '#000000');
            $.drawText($.santa.x, 10, 30, '#000000');
            $.drawText($.santa.y, 10, 40, '#000000');
        }
        
        $.drawText(Math.round($.time), 600/2-5, $.points.y, '#000000');
        $.ctx.drawImage($.clock, 600/2+20, $.points.y-16);
        
        $.drawText($.points.value, $.points.x, $.points.y, '#ffe200');
        $.ctx.drawImage($.gift.image, 205, 20, 20, 20, $.points.x+15, $.points.y-16, 20, 20);
        if($.points.rumble > 0) {
            if($.points.pointState === 'gain') {
                $.drawText("+1", $.santa.x+20 + $.points.rx-$.cameraX, $.santa.y+$.points.ry-10-$.cameraY, '#ffe200', $.plusFont);
            } else {
                $.drawText("-1", $.santa.x+20 + $.points.rx-$.cameraX, $.santa.y+$.points.ry-10-$.cameraY, '#FF0000', $.plusFont);
            }
        }
    }
    if($.gameState === 'score') {
        $.drawBackground();
        var text = "Santa brought " + $.points.value +" presents.";
        $.drawText(text, 200, 150, '#000000', $.normalFont);
        $.drawText("[Space] to go back to menu", 200, 200,'#000000');
    } 
}

$.drawBackground = function() {
    $.ctx.drawImage($.background, 0, $.bgy);
    if($.bgy != 0) {
        $.ctx.drawImage($.background, 0, $.bgy-$.canvas.height);
    }
    if($.bgy >= $.canvas.height) {
        $.bgy = 0;
    }
}

$.drawText = function(text, x, y, col, font) {
    $.ctx.font = $.normalFont;
    if(font) {
        $.ctx.font = font;
    }
    $.ctx.fillStyle = col;
    $.ctx.fillText(text, x, y);
}

$.handleInput = function() {
    // setup the pressed state for all keys
    for( var k in $.keys.state ) {
            if( $.keys.state[ k ] && !$.okeys[ k ] ) {
                    $.keys.pressed[ k ] = 1;
            } else {
                    $.keys.pressed[ k ] = 0;
            }
    }

    if($.gameState === 'menu') {
        if( $.keys.pressed["space"] && !$.spaceBlock) {
            $.resetGame();
            $.spaceBlock = true;
            $.gameState = 'game';
        }
    } 
    if($.gameState === 'game') {
        if( $.keys.pressed["right"] && !$.santa.isHurt) {
            if($.santa.xVel <= 5) {
                $.santa.xVel += 2;
            }
            $.santa.facingRight = true;
        }
        if( $.keys.pressed["left"] && !$.santa.isHurt) {
            if($.santa.xVel >= -5) {
                $.santa.xVel -= 2;
            }
            $.santa.facingRight = false;
        }
        if( $.keys.pressed["up"] && !$.santa.jump && !$.jumpblock) {
            $.santa.yVel -= 10;
            $.santa.jump = true;
            $.santa.animState = 2;
            $.audio.playSound($.audio.sounds['jump']);
            $.jumpblock = true;
        }
        if( !$.keys.pressed["up"] ) {
            $.jumpblock = false;
        }
        if( $.keys.pressed["space"] && $.gift.state === 'carried') {
            $.gift.state = 'thrown';
            $.gift.yvel = 10;
            if($.santa.moving) {
                $.gift.xvel = $.santa.facingRight ? 2 : -2;
            } else {
                $.gift.yvel = 0;
            }
            $.audio.playSound($.audio.sounds['throw']);
            $.spaceBlock = true;
        }
        
        if( $.keys.pressed["f"] && !$.fblock) {
            $.fblock = true;
            $.showFps = !$.showFps;
        } else if(!$.keys.pressed["f"]) {
            $.fblock = false;
        }
    }
    if($.gameState === 'score') {
        if( $.keys.pressed["space"] && !$.spaceBlock) {
            $.gameState = 'menu';
            $.spaceBlock = true;
        }
    } 
    if(!$.keys.pressed["space"]) {
        $.spaceBlock = false;
    }
}

/////////////////////////// EVENTS
$.keydowncb = function( e ) {
    var e = ( e.keyCode ? e.keyCode : e.which );
    if( e === 38 || e === 87 ){ $.keys.state.up = 1; }
    if( e === 39 || e === 68 ){ $.keys.state.right = 1; }
    if( e === 40 || e === 83 ){ $.keys.state.down = 1; }
    if( e === 37 || e === 65 ){ $.keys.state.left = 1; }
    if( e === 32 ){ $.keys.state.space = 1; }
    if( e === 70 ){ $.keys.state.f = 1; }
    if( e === 80 ){ $.keys.state.p = 1; }
}

$.keyupcb = function( e ) {
    var e = ( e.keyCode ? e.keyCode : e.which );
    if( e === 38 || e === 87 ){ $.keys.state.up = 0; }
    if( e === 39 || e === 68 ){ $.keys.state.right = 0; }
    if( e === 40 || e === 83 ){ $.keys.state.down = 0; }
    if( e === 37 || e === 65 ){ $.keys.state.left = 0; }
    if( e === 32 ){ $.keys.state.space = 0; }
    if( e === 70 ){ $.keys.state.f = 0; }
    if( e === 80 ){ $.keys.state.p = 0; }
}

window.onload = function () {
    $.init();
}
