$.menu = new function() {
    this.rumble = 0;
    this.rumbleX = 0;
    this.rumbleY = 0;
    this.title = new Image();
    this.title.src = "res/title.png";
    
    this.update = function() {
        if(this.rumble > 0) {
            this.rumble--;
            this.rumble = ($.rumble < 0) ? 0 : this.rumble;
            this.rumbleX = $.utils.rand(-this.rumble, this.rumble);
            this.rumbleY = $.utils.rand(-this.rumble, this.rumble);
        }
    }
    
    this.render = function() {
        $.ctx.drawImage(this.title, 600/2-250, 360/2-this.title.height-10, 500, 250)
        $.drawText("[Space] to play", 600/2-60, 360/2+100,'#000000');
    }
}
