$.Cloud = function(x,y,visibleY) {   
    this.x = x;
    this.y = y;
    this.visible = false;
    this.visibleY = visibleY;
};

$.Cloud.prototype.update = function() {
    this.y -= 2;
    if(this.y <= this.visibleY) {
        this.visible = true;
    }
};

$.Cloud.prototype.render = function() {
    if(this.visible) {
        // only draw cloud if it's visible on camera
        if($.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x, this.y) || 
           $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x+64, this.y) || 
           $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x, this.y+64) || 
           $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, this.x+64, this.y+64) ) {
            $.ctx.drawImage($.cloudImage, this.x-$.cameraX, this.y-$.cameraY);
        }
    }
};
