$.Level = function() {
    this.width = 25; // 1600 px wide
    this.height = 7; // 256 px high
    this.tileSize = 64;
    this.pxWidth = this.width * this.tileSize;
    this.pxHeight = this.height * this.tileSize;
    this.layout = [
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,4,0,0,4,0,0,0],
        [2,3,0,1,2,2,2,2,3,0,1,3,0,1,2,2,2,2,3,0,0,1,2,2,2]
    ];
    this.textures = new Image();
    this.textures.src = "res/textures.png";
    
	this.chimneys = [];
    this.chimneys.push( new $.Chimney(4*64, 320) );
    this.chimneys.push( new $.Chimney(6*64, 320) );
    this.chimneys.push( new $.Chimney(708, 320) );
    this.chimneys.push( new $.Chimney(15*64, 320) );
    this.chimneys.push( new $.Chimney(24*64, 320) );
    
    this.giftPlaces   = [194, 34, 324, 644, 1347, 476, 891, 1054];
    
    this.update = function() {
        for( var c=$.clouds.length-1; c>=0; c--) {
            $.clouds[c].update();
            if( $.clouds[c].y+64 < 0) {
                $.clouds.splice(c, 1);
            }
        }
        
        for( ch in this.chimneys ) {
            this.chimneys[ch].update();
        }
    }
    
    this.render = function() {
        for( var y=0; y<this.height; y++ ) {
            for( var x=0; x<this.width; x++) {
                // 0 = nothing, but texture is of course zero-based
                var type = this.layout[y][x] - 1;
                var xx = x*64 - $.cameraX;
                var yy = y*64 - $.cameraY;
                if(type < 0) {
                    continue;
                }
                // only render if tile is visible
                if($.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, x*64, y*64) || 
                   $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, x*64+this.tileSize, y*64) || 
                   $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, x*64, y*64+this.tileSize) || 
                   $.utils.pointInRect($.cameraX, $.cameraY, $.canvas.width, $.canvas.height, x*64+this.tileSize, y*64+this.tileSize) ) {
                    $.ctx.drawImage(this.textures, type * 64,0,64,64, xx,yy,64,64);
                }
            }
        }
        for(var c in this.chimneys) {
            this.chimneys[c].render();
        }
    };
    
    this.walkable = function(x,y) {
        if( x < 0 || x > this.pxWidth-1) {
            return false;
        }
        if( y < 0 || y > this.pxHeight-1) {
            return false;
        }
        for(var c in this.chimneys) {
            if($.utils.pointInRect(this.chimneys[c].x+10, this.chimneys[c].y+10, 44, 64, x, y)) {
                return false;
            }
        }
        var xx = Math.floor(x/this.tileSize);
        var yy = Math.floor(y/this.tileSize);
        return (this.layout[yy][xx] === 0)
    }
};
