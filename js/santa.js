$.Santa = function() {
    this.image = new Image();
    this.image.src = "res/santaanim.png";
    this.reset();
};

$.Santa.prototype.reset = function() {
	this.x = 0;
    this.y = $.level.pxHeight-160;
    this.xVel = 0;
	this.yVel = 0;
    this.jump = false;
    this.animTimer = 0;
    this.animState = 0;
    this.moving = false;
    this.facingRight = true;
    this.isHurt = false;
    this.hurtTimer = 0;
};

$.Santa.prototype.update = function() {
    // update animation
    this.animTimer++;
    if( this.animTimer >= 5 && !this.jump && this.moving ) {
        this.animState = (this.animState === 0 ? 1 : 0);
        this.animTimer = 0;
    }
    
    // ugly hack for now: santa can't jump out
    // of the bottom of the screen, to we 'kill'
    // him 5 px before
    if(this.y+64 >= $.level.pxHeight-5) {
        this.reset();
        $.points.losePoint();
        $.gift.reset();
    }
    
    if(this.isHurt) {
        this.hurtTimer--;
        if(this.hurtTimer < 0) {
            this.isHurt = false;
        }
    } else {
        for(var c in $.clouds) {
            if(!$.clouds[c].visible) {
                continue;
            }
            if($.utils.rectInRect(this.x+10, this.y, 54, 64, $.clouds[c].x+10, $.clouds[c].y+10, 54, 54)) {
                if(this.x < $.clouds[c].x) {
                    this.xVel = -7;
                } else {
                    this.xVel = 7;
                }
                this.yVel -= 5;
                $.audio.playSound($.audio.sounds['hurt']);
                this.isHurt = true;
                this.hurtTimer = 20;
            }
        }
    }
    
    // update movement
    this.moving = false;
    if(this.xVel != 0) {
        this.xVel += (this.xVel < 0 ? 0.5 : -0.5);
        this.moving = true;
    } else if(!this.jump) {
        this.animState = 0;
        this.animTimer = 0;
    }
    if(Math.abs(this.xVel) < 0.5) {
        this.xVel = 0;
    }
    if(this.xVel != 0) {
        // we have to check both top right and bottom right corner
        // of bounding box
        var ty = this.y;
        var tyb = this.y + 64;
        var tx;
        if(this.xVel < 0) {
            tx = this.x+20+this.xVel;
        } else {
            tx = this.x-20+this.xVel+64;
        }
        if($.level.walkable(tx, ty) && $.level.walkable(tx, tyb)) {
            this.x += this.xVel;
        } else {
            this.xVel = 0;
        }
    }
    
    // update jump logic
    this.yVel += 0.5;
    if(this.yVel > 15) {
        this.yVel = 15;
    }
    if(this.yVel != 0) {
        // we have to check both bottom left and bottom right
        // corner of bounding box
        var tx = this.x+20
        var txr = (this.x-20)+ 64
        var ty;
        if(this.yVel < 0) {
            ty = this.y + this.yVel
        } else {
            ty = this.y + this.yVel + 64
        }
        if($.level.walkable(tx, ty) && $.level.walkable(txr, ty)) { 
            this.y += this.yVel;
        } else {
            if(this.yVel > 0) {
                this.yVel = 0;
                if(this.jump) {
                    this.jump = false;
                    this.animState = 0;
                }
            } else {
                this.yVel += Math.abs(this.yVel);
            }
        }
    }
};

$.Santa.prototype.render = function() {
    if(this.facingRight) {
        $.ctx.drawImage(this.image, this.animState * 64,0,64,64, this.x-$.cameraX,this.y-$.cameraY,64,64);
    } else {
        $.ctx.drawImage(this.image, this.animState * 64,64,64,64, this.x-$.cameraX,this.y-$.cameraY,64,64);
    }
};
