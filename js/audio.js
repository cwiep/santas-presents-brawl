$.audio = {};

$.audio.sounds = {
    'jump': [0,,0.07,,0.21,0.3607,,0.1751,,,,,,0.5028,,,,,1,,,,,0.5],
    'gift': [0,,0.1384,,0.1891,0.2456,,0.327,,,,,,0.5097,,0.6187,,,1,,,,,0.5],
    'throw': [0,,0.2024,0.0568,0.139,0.4304,0.0811,-0.496,,,,,,0.7365,-0.3519,,0.1224,-0.1636,1,,,,,0.5],
    'point': [0,,0.0995,0.4311,0.3525,0.647,,,,,,0.44,0.6378,,,,,,1,,,,,0.5],
    'minuspoint': [0,,0.0844,,0.2606,0.5542,,-0.555,,,,,,0.146,,,,,1,,,,,0.5],
    'hurt': [0,,0.0869,,0.2879,0.6707,,-0.424,,,,,,0.1821,,,,,1,,,0.1877,,0.5]
};

$.audio.playSound = function(params) {
    try {
        var soundURL = jsfxr(params);
        var player = new Audio();
        player.addEventListener('error', function(e) {
            console.log("Error: " + player.error.code);
        }, false);
        player.src = soundURL;
        player.play();
        } catch(e) {
            console.log(e.message);
    }
}
