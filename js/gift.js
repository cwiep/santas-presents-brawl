$.Gift = function() {
    this.image = new Image();
    this.image.src = "res/gift.png";
    
    this.x = $.level.giftPlaces[$.curGift];
    this.y = 0;
    this.xvel = 0;
    this.yvel = 5;
    this.state = 'falling';
    this.animTimer = 0;
    this.animState = 0;
    
    this.reset = function() {
        this.x = $.level.giftPlaces[$.curGift];
        this.y = 0;
        this.yvel = 5;
        this.state = 'falling';
        this.animState = 0;
    }
    
    this.nextGift = function() {
        $.curGift = ~~($.utils.rand(0, $.level.giftPlaces.length));
        $.curChimney = ~~($.utils.rand(0, $.level.chimneys.length));
        this.reset();
        $.points.gainPoint();
    }
    
    this.update = function() {
        if(this.state === 'falling') {
            if($.level.walkable(this.x+10, this.y + 54 + this.yvel) && $.level.walkable(this.x+54, this.y +54 + this.yvel)) {
                this.y += this.yvel;
            } else {
                this.state = 'resting';
                this.yvel = 0;
            }
        }
        if(this.state === 'resting' || this.state === 'falling') {
            if( $.utils.rectInRect($.santa.x, $.santa.y, 64, 64, this.x, this.y, 64, 64) ) {
                this.state = 'carried';
                $.audio.playSound($.audio.sounds['gift']);
            }
        }
        if(this.state === 'resting') {
            this.animTimer--;
            if(this.animTimer < 0) {
                this.animTimer = 20;
                this.animState = !this.animState;
            }
        }
        if(this.state === 'carried') {
            if($.santa.facingRight) {
                this.x = $.santa.x+32;
                this.y = $.santa.y+10;
            } else {
                this.x = $.santa.x-32;
                this.y = $.santa.y+10;
            }
        }
        if(this.state === 'thrown') {
            this.x += this.xvel;
            this.y -= this.yvel;
            this.yvel--;
            
            if($.utils.rectInRect(this.x+10, this.y+10, 54, 54, $.level.chimneys[$.curChimney].x+15, $.level.chimneys[$.curChimney].y, 49, 20)) {
                this.x = $.level.chimneys[$.curChimney].x;
                this.y = $.level.chimneys[$.curChimney].y-40;
                this.animTimer = 10;
                this.state = 'point0';
            }
            if(this.y > $.level.pxHeight) {
                this.reset();
                $.points.losePoint();
            }
        }
        if(this.state === 'point0') {
            this.animState = 0;
            this.animTimer = 10;
            this.state = 'point1';
        }
        if(this.state === 'point1' ) {
            this.animTimer--;
            if(this.animTimer < 0) {
                this.animState = 2;
                this.animTimer = 10;
                this.state = 'point2';
            }
        }
        if(this.state === 'point2' ) {
            this.animTimer--;
            if(this.animTimer < 0) {
                $.level.chimneys[$.curChimney].animTimer = 10;
                $.level.chimneys[$.curChimney].state = 1;
                this.nextGift();
            }
        }
    }
    
    this.render = function() {
        $.ctx.drawImage(this.image, this.animState*64, 0, 64, 64, this.x-$.cameraX, this.y-$.cameraY, 64, 64);
    }
};
